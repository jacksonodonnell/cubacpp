#include "catch2/catch.hpp"
#include "cubacpp/gsl.hh"
#include "cubacpp/integrand.hh"
#include <iostream>

TEST_CASE("Scalar 1D integrand can be called", "[integrand]")
{
  auto f1 = [](double x) { return 2. * x; };
  auto i1 = cubacpp::detail::integrand<decltype(f1)>;
  double res = 0.0;
  int const ndim = 1;
  double const x = 3.5;
  int const ncomp = 1;
  i1(&ndim, &x, &ncomp, &res, &f1);
  REQUIRE(res == 7.0);
}

TEST_CASE("Scalar 2D integrand can be called", "[integrand]")
{
  auto f2 = [](double x, double y) { return x + y; };
  auto i2 = cubacpp::detail::integrand<decltype(f2)>;
  double res = 0.0;
  int const ndim = 2;
  double const x[] = {2.0, -3.0};
  int const ncomp = 1;
  i2(&ndim, x, &ncomp, &res, &f2);
  REQUIRE(res == -1.0);
}

TEST_CASE("2D vector 1D integrand can be called", "[integrand]")
{
  auto f = [](double x) { return std::array<double, 2>{{-x, x}}; };
  auto igrand = cubacpp::detail::integrand<decltype(f)>;
  double res[] = {0.0, 0.0};
  REQUIRE(sizeof(res) == 2 * sizeof(double));
  double const x = 5.0;
  int const ndim = 1;
  int const ncomp = 2;
  igrand(&ndim, &x, &ncomp, res, &f);
  REQUIRE(res[0] == -5.0);
  REQUIRE(res[1] == 5.0);
}

TEST_CASE("2D vector 3D integrand can be called", "[integrand]")
{
  auto f = [](double x, double y, double z) {
    return std::array<double, 2>{{x + y, y + z}};
  };
  auto igrand = cubacpp::detail::integrand<decltype(f)>;
  double res[] = {0.0, 0.0};
  double const x[] = {1.0, 2.0, 3.0};
  int const ndim = sizeof(x) / sizeof(double);
  int const ncomp = sizeof(res) / sizeof(double);
  igrand(&ndim, x, &ncomp, res, &f);
  REQUIRE(res[0] == 3.0);
  REQUIRE(res[1] == 5.0);
}

TEST_CASE("2D vector 2D integrand returning std::vector can be called",
          "[integrand]")
{
  auto f = [](double x, double y) {
    return std::vector<double>{{x + y, x - y}};
  };
  auto igrand = cubacpp::detail::integrand<decltype(f)>;
  double res[] = {0., 0.};
  double const x[] = {3., 2.};
  int const ndim = sizeof(x) / sizeof(double);
  int const ncomp = 2;
  igrand(&ndim, x, &ncomp, res, &f);
  REQUIRE(res[0] == 5.0);
  REQUIRE(res[1] == 1.0);
}

TEST_CASE("Test gsl_function wrapper", "[integrand]")
{
  SECTION("minimal example")
  {
    auto f = [](double a) { return a * a + a; };
    auto igrand = cubacpp::detail::make_gsl_integrand(&f);
    REQUIRE(igrand.function(0.0, igrand.params) == 0.0);
    REQUIRE(igrand.function(1.0, igrand.params) == 2.0);
  }

  SECTION("polynomial")
  {
    for (auto i = 0; i < 10; i++) {
      for (auto j = 0; j < 10; j++) {
        const double x = ((double)i) / 10.0, y = ((double)j) / 10.0,
                     a2 = 2 * x + y, a1 = 3 * x * y - 3,
                     a0 = x / (y + 1) + x * x * 4;

        auto f = [a2, a1, a0](double a) { return a2 * a * a + a1 * a + a0; };
        auto igrand = cubacpp::detail::make_gsl_integrand(&f);
        CHECK(igrand.function(1, igrand.params) == Approx(a2 + a1 + a0));
      }
    }
  }
}
