#ifndef CUBACPP_VEGAS_HH
#define CUBACPP_VEGAS_HH

#include "cuba.h"
#include "cubacpp/arity.hh"
#include "cubacpp/integrand.hh"
#include "cubacpp/integrand_traits.hh"
#include "cubacpp/integration_result.hh"

namespace cubacpp {

  template <typename F>
  auto
  VegasIntegrate(F const& f,
                 double epsrel,
                 double epsabs,
                 int flags = 0,
                 long long mineval = 0,
                 long long maxeval = 50000,
                 long long nstart = 1000,
                 long long nincrease = 500,
                 long long nbatch = 1000)
  {
    integrand_t igrand = detail::integrand<F>;
    constexpr int nvec = 1;

    auto res = detail::default_construct(f);
    auto [nc, pvals, perrs, pprobs] = detail::make_cuba_args(f, res);
    res.nregions = -1;

    llVegas(detail::integrand_traits<F>::ndim,
            nc,
            igrand,
            (void*)&f,
            nvec,
            epsrel,
            epsabs,
            flags,
            0, // seed
            mineval,
            maxeval,
            nstart,
            nincrease,
            nbatch,
            0,
            nullptr,
            nullptr,
            &res.neval,
            &res.status,
            pvals,
            perrs,
            pprobs);
    return res;
  }

  struct Vegas {
    int flags = 0;
    long long int mineval = 0;
    long long int maxeval = 50000;
    long long nstart = 1000;
    long long nincrease = 500;
    long long nbatch = 1000;

    template <typename F>
    auto
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return VegasIntegrate(
        f, epsrel, epsabs, flags, mineval, maxeval, nstart, nincrease, nbatch);
    }
  };
}

#endif
