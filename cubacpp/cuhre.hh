#ifndef CUBACPP_CUHRE_HH
#define CUBACPP_CUHRE_HH

#include "cuba.h"
#include "cubacpp/arity.hh"
#include "cubacpp/integrand.hh"
#include "cubacpp/integrand_traits.hh"
#include "cubacpp/integration_result.hh"

namespace cubacpp {

  template <typename F>
  auto
  CuhreIntegrate(F const& f,
                 double epsrel,
                 double epsabs,
                 int flags = 0,
                 long long mineval = 0,
                 long long maxeval = 50000,
                 int key = -1)
  {
    using results_type =
      typename detail::integrand_traits<F>::integration_results_type;

    // If key was not specified, deduce the highest order we can use based on
    // the dimensionality of the integrand.
    constexpr auto N = detail::integrand_traits<F>::ndim;
    static_assert(N >= 2,
                  "Cuhre requires an integrand dimension of two or more");
    if (key < 0) {
      switch (N) {
        case 1:
        case 2:
          key = 13;
          break;
        case 3:
          key = 11;
          break;
        default:
          key = 9;
      }
    }
    if (key != 7 && key != 9 && key != 11 && key != 13) {
      results_type res;
      res.status = -2;
      return res;
    }
    integrand_t igrand = detail::integrand<F>;
    constexpr int nvec = 1;

    auto res = detail::default_construct(f);

    auto [nc, pvals, perrs, pprobs] = detail::make_cuba_args(f, res);

    llCuhre(detail::integrand_traits<F>::ndim,
            nc,
            igrand,
            (void*)&f,
            nvec,
            epsrel,
            epsabs,
            flags,
            mineval,
            maxeval,
            key,
            nullptr,
            nullptr,
            &res.nregions,
            &res.neval,
            &res.status,
            pvals,
            perrs,
            pprobs);
    return res;
  }

  struct Cuhre {
    int flags = 0;
    long long int mineval = 0;
    long long int maxeval = 50000;
    int key = -1;

    template <typename F>
    auto
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return CuhreIntegrate(f, epsrel, epsabs, flags, mineval, maxeval, key);
    }
  };
}

#endif
