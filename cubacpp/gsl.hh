#ifndef CUBACPP_GSL_HH
#define CUBACPP_GSL_HH

#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>

#include "cubacpp/arity.hh"
#include "cubacpp/integrand.hh"
#include "cubacpp/integrand_traits.hh"
#include "cubacpp/integration_result.hh"

namespace cubacpp {
  namespace detail {
    // GSL takes a different integrand format from Cuba - this and the following
    // function will help that
    template <class F>
    double
    gsl_integrand(double x, void* params)
    {
      F* f = reinterpret_cast<F*>(params);
      return (*f)(x);
    }

    template <typename F>
    gsl_function
    make_gsl_integrand(F const* f)
    {
      using fn = detail::integrand_traits<F>;
      static_assert(fn::ndim == 1, "GSL Integrals only support 1D");
      static_assert(
        std::is_same<typename fn::function_return_type, double>::value,
        "GSL Integrals only support 1D");
      gsl_function out;
      out.function = static_cast<double (*)(double, void*)>(gsl_integrand<F>);
      out.params = const_cast<void*>(reinterpret_cast<void const*>(f));
      return out;
    }
  }

  // QNG - Quadrature, Non-Adaptive, General
  // Uses Gauss-Konrod rules to evaluate at a maximum of 81 points
  template <class F>
  integration_results<1>
  QNGIntegrate(F const& f,
               double a,
               double b,
               double epsrel,
               double epsabs)
  {
    // If key was not specified, deduce the highest order we can use based on
    // the dimensionality of the integrand.
    auto igrand = detail::make_gsl_integrand<F>(&f);
    double val, err;
    std::size_t neval;
    // Possible error codes:
    //  GSL_EMAXITER
    //  GSL_EROUND
    //  GSL_ESING
    //  GSL_EDIVERGE
    //  GSL_EDOM
    int retval = gsl_integration_qng(
      &igrand, a, b, epsabs, epsrel, &val, &err, &neval);
    // TODO: give sensible values for probability, neval, etc
    return {val, err, 0.0, (long long) neval, 0, retval == GSL_SUCCESS ? 0 : 1};
  }

  // QAG - Quadrature, Adaptive, General
  template <class F>
  integration_results<1>
  QAGIntegrate(F const& f,
               gsl_integration_workspace* gsl_wkspc,
               double a,
               double b,
               double epsrel,
               double epsabs,
               int key = 1,
               std::size_t limit = 50000)
  {
    // If key was not specified, deduce the highest order we can use based on
    // the dimensionality of the integrand.
    auto igrand = detail::make_gsl_integrand<F>(&f);
    double val, err;
    // Possible error codes:
    //  GSL_EMAXITER
    //  GSL_EROUND
    //  GSL_ESING
    //  GSL_EDIVERGE
    //  GSL_EDOM
    int retval = gsl_integration_qag(
      &igrand, a, b, epsabs, epsrel, limit, key, gsl_wkspc, &val, &err);
    // TODO: give sensible values for probability, neval, etc
    return {val, err, 0.0, 0, 0, retval == GSL_SUCCESS ? 0 : 1};
  }

  template <class F>
  integration_results<1>
  CQUADIntegrate(F const& f,
                 gsl_integration_cquad_workspace* gsl_wkspc,
                 double a,
                 double b,
                 double epsrel,
                 double epsabs)
  {
    // If key was not specified, deduce the highest order we can use based on
    // the dimensionality of the integrand.
    auto igrand = detail::make_gsl_integrand<F>(&f);
    double val, err;
    std::size_t neval;
    // Possible error codes:
    //  GSL_EMAXITER
    //  GSL_EROUND
    //  GSL_ESING
    //  GSL_EDIVERGE
    //  GSL_EDOM
    int retval = gsl_integration_cquad(
      &igrand, a, b, epsabs, epsrel, gsl_wkspc, &val, &err, &neval);
    // TODO: give sensible values for probability, neval, etc
    return {val, err, 0.0, 0, (int) neval, retval == GSL_SUCCESS ? 0 : 1};
  }

  /* Unlike Cuba, the GSL integration routines integrate an explicit range
   * (rather than across the unit hypercube). It will integrate from
   * `range_start` to `range_end`.
   */
  struct GSLIntegrator {
    double range_start, range_end;

    GSLIntegrator(double range_start, double range_end)
      : range_start(range_start)
      , range_end(range_end)
    {}

    // Helper member to ease changing integration ranges
    GSLIntegrator&
    with_range(double start, double end)
    {
      range_start = start;
      range_end = end;
      return *this;
    }
  };

  struct GSLWorkspaceIntegrator : GSLIntegrator {
  protected:
    gsl_integration_workspace* wkspc;

  public:
    size_t limit = 0;

    GSLWorkspaceIntegrator(double range_start, double range_end, std::size_t n)
      : GSLIntegrator(range_start, range_end)
      , wkspc(gsl_integration_workspace_alloc(n))
      , limit(n)
    {}

    GSLWorkspaceIntegrator(const GSLWorkspaceIntegrator& other)
      : GSLWorkspaceIntegrator(other.range_start, other.range_end, other.limit)
    {}

    GSLWorkspaceIntegrator(GSLWorkspaceIntegrator&& other)
      : GSLIntegrator(other.range_start, other.range_end)
      , wkspc(other.wkspc), limit(other.limit)
    {}

    ~GSLWorkspaceIntegrator()
    {
      if (wkspc)
        gsl_integration_workspace_free(wkspc);
    }
  };

  // QNG - Quadrature, Non-Adaptive, General
  // See GSL docs:
  // https://www.gnu.org/software/gsl/doc/html/integration.html#c.gsl_integration_qng
  struct QNG : GSLIntegrator {
    /* QNG will apply successive Gauss-Konrod rules of 10, 21, 43, or at most,
     * 87 points. If the error does not converge at 87 points, the integration
     * fails.
     */
    QNG(double range_start = 0,
        double range_end = 1)
      : GSLIntegrator(range_start, range_end)
    {}

    QNG&
    with_range(double start, double end)
    {
      GSLIntegrator::with_range(start, end);
      return *this;
    }

    template <class F>
    integration_results<detail::ncomp<F>()>
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return QNGIntegrate(
        f, range_start, range_end, epsrel, epsabs);
    }
  };

  // QAG - Quadrature, Adaptive, General
  // See GSL docs:
  // https://www.gnu.org/software/gsl/doc/html/integration.html#cquad-doubly-adaptive-integration
  struct QAG : public GSLWorkspaceIntegrator {
    int key = -1;

    /* The integration rule is determined by the value of key, which should be
     * chosen from the following symbolic names,
     *
     * GSL_INTEG_GAUSS15 (key = 1)
     * GSL_INTEG_GAUSS21 (key = 2)
     * GSL_INTEG_GAUSS31 (key = 3)
     * GSL_INTEG_GAUSS41 (key = 4)
     * GSL_INTEG_GAUSS51 (key = 5)
     * GSL_INTEG_GAUSS61 (key = 6)
     *
     * corresponding to the 15, 21, 31, 41, 51 and 61 point Gauss-Kronrod rules.
     */
    QAG(double range_start = 0,
        double range_end = 1,
        int key = GSL_INTEG_GAUSS61,
        std::size_t limit = 10)
      : GSLWorkspaceIntegrator(range_start, range_end, limit)
      , key(key)
    {}

    QAG&
    with_range(double start, double end)
    {
      GSLIntegrator::with_range(start, end);
      return *this;
    }

    template <class F>
    integration_results<detail::ncomp<F>()>
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return QAGIntegrate(
        f, wkspc, range_start, range_end, epsrel, epsabs, key, limit);
    }
  };

  // CQUAD doubly-adaptive integration
  // See GSL docs:
  // https://www.gnu.org/software/gsl/doc/html/integration.html#cquad-doubly-adaptive-integration
  struct CQUAD : GSLIntegrator {
  protected:
    gsl_integration_cquad_workspace* wkspc;
    std::size_t nintervals;

  public:
    CQUAD(double range_start = 0.0,
          double range_end = 1.0,
          std::size_t n = 100)
      : GSLIntegrator(range_start, range_end)
      , wkspc(gsl_integration_cquad_workspace_alloc(n))
      , nintervals(n)
    {}

    CQUAD(const CQUAD& other)
      : CQUAD(other.range_start, other.range_end, other.nintervals) {}

    CQUAD(CQUAD&& other)
      : GSLIntegrator(other.range_start, other.range_end)
      , wkspc(other.wkspc)
      , nintervals(other.nintervals)
    {}

    ~CQUAD()
    {
      if (wkspc)
        gsl_integration_cquad_workspace_free(wkspc);
    }

    CQUAD&
    with_range(double start, double end)
    {
      GSLIntegrator::with_range(start, end);
      return *this;
    }

    template <class F>
    integration_results<detail::ncomp<F>()>
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return CQUADIntegrate(f, wkspc, range_start, range_end, epsrel, epsabs);
    }
  };
}

#endif
