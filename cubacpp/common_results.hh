#ifndef CUBACPP_ABS_RESULTS_HH
#define CUBACPP_ABS_RESULTS_HH

namespace cubacpp {

  // common_results contains the common part of all integration results. It is
  // intended to be used as a base for other results classes.

  struct common_results {

    long long neval = 0;
    int nregions = -1;
    int status = 1;

    bool
    converged() const
    {
      return status == 0;
    }

    common_results(long long neval, int nregions, int status)
      : neval(neval), nregions(nregions), status(status)
    {}

    common_results() = default;
  };

}

#endif // CUBACPP_ABS_RESULTS_HH
