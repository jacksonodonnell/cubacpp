#ifndef CUBACPP_INTEGRAND_HH
#define CUBACPP_INTEGRAND_HH

#include "cubacpp/integrand_traits.hh"
#include <array>
#include <cstddef>
#include <stdexcept>
#include <tuple> // for std::apply
#include <vector>

namespace cubacpp::detail {

  template <std::size_t N>
  std::array<double, N>
  to_array(double const* x)
  {
    std::array<double, N> res;
    for (std::size_t i = 0; i < N; ++i)
      res[i] = x[i];
    return res;
  }

  template <typename F>
  int
  integrand(int const* ndim,
            double const x[],
            [[maybe_unused]] int const* ncomp,
            double f[],
            void* obj)
  {
    // N is the number of arguments the function F expects.
    constexpr auto N = integrand_traits<F>::ndim;
    if (*ndim != N)
      return -999;
    F const* const fcn = reinterpret_cast<F const*>(obj);

    // The type of 'res' will be:
    //    std::array<double, n> or
    //    std::vector<double> or
    //    double
    auto const res = std::apply(*fcn, to_array<N>(x));

    if constexpr (std::is_same_v<decltype(res), double const>) {
      *f = res;
    } else {
      if (static_cast<std::size_t>(*ncomp) != res.size())
        return -999;
      for (auto const val : res) {
        *f = val;
        ++f;
      }
    };
    return 0;
  }
}

#endif
