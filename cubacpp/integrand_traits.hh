#ifndef CUBACPP_INTEGRAND_TRAITS_HH
#define CUBACPP_INTEGRAND_TRAITS_HH

#include "cubacpp/arity.hh"
#include "cubacpp/integration_result.hh"
#include <array>
#include <tuple> // for std::apply
#include <vector>

namespace cubacpp {

  namespace detail {

    // The function template integrand is to be used, in conjunction with a
    // user-supplied class F, to provide the C-function to be passed to one of
    // the CUBA library's integration routines.
    template <typename F>
    int integrand(int const*, double const*, int const*, double* f, void*);

    // integration_results_type_for<T>::type is the return type for integration
    // routines when integrating a function with return type T. No general case
    // is supplied; there are only specializations.
    template <typename>
    struct integration_results_type_for;

    template <>
    struct integration_results_type_for<std::vector<double>> {
      using type = integration_results_v;
    };

    template <>
    struct integration_results_type_for<double> {
      using type = integration_result;
    };

    template <std::size_t N>
    struct integration_results_type_for<std::array<double, N>> {
      using type = integration_results<N>;
    };

    // integrand_traits<F> provides information about the callable type
    // F.
    // The static data member 'ndim' reports the number of arguments
    // of the callable object (the dimensionality of the integral to be
    // calculated).
    //
    // The nested type 'function_return_type' reports the return
    // type of the call. Expected types are 'double', or 'std::array<double, N>'
    // for any positive integral N.
    //
    // The nested type 'integration_results_type' reports the type returned from
    // integrating this integrand.
    //
    // The nested type 'arg_type' reports the array typed used to pass arguments
    // to the wrapped function.
    template <typename F>
    struct integrand_traits {
      static constexpr auto ndim = cubacpp::util::arity<F>();

      using function_return_type =
        decltype(std::apply(std::declval<F>(), std::array<double, ndim>()));

      using integration_results_type =
        typename integration_results_type_for<function_return_type>::type;

      using arg_type = std::array<double, ndim>;
    };

    template <typename F>
    std::size_t
    runtime_ncomp(F f)
    {
      typename integrand_traits<F>::arg_type args{};
      return std::apply(f, args).size();
    };

    template <typename F>
    constexpr std::size_t
    ncomp()
    {
      using rt = typename integrand_traits<F>::function_return_type;
      return sizeof(rt) / sizeof(double);
    };

    template <typename F>
    static typename integrand_traits<F>::integration_results_type
    default_construct([[maybe_unused]] F f)
    {
      using rtype = typename integrand_traits<F>::integration_results_type;
      using ftype = typename integrand_traits<F>::function_return_type;
      if constexpr (std::is_same_v<ftype, std::vector<double>>) {
        return rtype(runtime_ncomp(f));
      } else {
        return rtype{};
      }
    }

    template <typename F, typename RES>
    std::tuple<std::size_t, double*, double*, double*>
    make_cuba_args(F f, RES& result)
    {
      if constexpr (std::is_same_v<typename detail::integrand_traits<decltype(
                                     f)>::function_return_type,
                                   std::vector<double>>) {
        return std::make_tuple(detail::runtime_ncomp(f),
                               result.value.data(),
                               result.error.data(),
                               result.prob.data());
      } else {
        return std::make_tuple(detail::ncomp<F>(),
                               (double*)&result.value,
                               (double*)&result.error,
                               (double*)&result.prob);
      }
    }

  }
}
#endif
