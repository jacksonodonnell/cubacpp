#ifndef CUBACPP_SUAVE_HH
#define CUBACPP_SUAVE_HH

#include "cuba.h"
#include "cubacpp/arity.hh"
#include "cubacpp/integrand.hh"
#include "cubacpp/integrand_traits.hh"
#include "cubacpp/integration_result.hh"

namespace cubacpp {

  template <typename F>
  auto
  SuaveIntegrate(F const& f,
                 double epsrel,
                 double epsabs,
                 int flags = 0,
                 long long mineval = 0,
                 long long maxeval = 50000,
                 long long nnew = 1000,
                 long long nmin = 2,
                 double flatness = 25)
  {
    integrand_t igrand = detail::integrand<F>;
    constexpr int nvec = 1;

    auto res = detail::default_construct(f);
    auto [nc, pvals, perrs, pprobs] = detail::make_cuba_args(f, res);

    llSuave(detail::integrand_traits<F>::ndim,
            nc,
            igrand,
            (void*)&f,
            nvec,
            epsrel,
            epsabs,
            flags,
            0, // seed
            mineval,
            maxeval,
            nnew,
            nmin,
            flatness,
            nullptr,
            nullptr,
            &res.nregions,
            &res.neval,
            &res.status,
            pvals,
            perrs,
            pprobs);
    return res;
  }

  struct Suave {
    int flags = 0;
    long long int mineval = 0;
    long long int maxeval = 50000;
    long long int nnew = 1000;
    long long int nmin = 2;
    double flatness = 25.0;

    template <typename F>
    auto
    integrate(F const& f, double epsrel, double epsabs) const
    {
      return SuaveIntegrate(
        f, epsrel, epsabs, flags, mineval, maxeval, nnew, nmin, flatness);
    }
  };
}

#endif
